#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <errno.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <map>
#include <sys/stat.h>
#include <arpa/inet.h>
#define MAX 80 
#define PORT 8080 
#define SEND_PORT 5050
#define SA struct sockaddr 
#define MAX_TCP_PACKET_SIZE 65535
#define USER "user"
#define PASS "pass"

#define FILENAME 

#ifndef __cplusplus
extern "C"
{
#endif
// Function designed for chat between client and server. 

void func(int sockfd);
// int tconnect(char* IP, char *User, char *Pass);
// int tput();
// int tget();

// int tconnect(char* IP, char *User, char* Pass){

// }
// void func(int sockfd) 
// { 
// 	char buff[MAX]; 
// 	int n; 
// 	// infinite loop for chat 
// 	for (;;) { 
// 		bzero(buff, MAX); 

// 		// read the message from client and copy it in buffer 
// 		read(sockfd, buff, sizeof(buff)); 
// 		// print buffer which contains the client contents 
// 		printf("From client: %s\t To client : ", buff); 
// 		bzero(buff, MAX); 
// 		n = 0; 
// 		// copy server message in the buffer 
// 		while ((buff[n++] = getchar()) != '\n') 
// 			; 

// 		// and send that buffer to client 
// 		write(sockfd, buff, sizeof(buff)); 

// 		// if msg contains "Exit" then server exit and chat ended. 
// 		if (strncmp("exit", buff, 4) == 0) { 
// 			printf("Server Exit...\n"); 
// 			break; 
// 		} 
// 	} 
// } 

//void tConnect(int sockfd, sockaddr_in servaddr){
//	printf("Connected to Client: %s", ClientIP);

	// connect the client socket to server socket 
	// if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
	// 	printf("connection with the server failed...\n"); 
	// 	exit(EXIT_FAILURE); 
	// } 
	// else
	// 	printf("connected to the server..\n"); 

//}

// Main function 
int main() { 
	int sockfd, connfd, b; 
	socklen_t len;
	struct sockaddr_in servaddr, cli; 

	char buff[MAX_TCP_PACKET_SIZE];

	// socket create and verification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("socket creation failed...\n"); 
		exit(EXIT_FAILURE); 
	} 
	else
		printf("Socket successfully created..\n"); 
	bzero(&servaddr, sizeof(servaddr)); 

	// Server Address Definition
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(PORT); 

	// Binding newly created socket to given IP and verification 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
		printf("socket bind failed...\n"); 
		exit(EXIT_FAILURE); 
	} 
	else
		printf("Socket successfully binded..\n"); 

	// Now server is ready to listen and verification 
	if ((listen(sockfd, 10)) != 0) { 
		printf("Listen failed...\n"); 
		exit(0); 
	} 
	else
		printf("Server listening..\n"); 
	len = sizeof(servaddr); 

	// Accept the data packet from client and verification 
	while(1){
		connfd = accept(sockfd, (SA*)&servaddr, &len); 
		if (connfd < 0) { 
			printf("server acccept failed...\n"); 
			exit(EXIT_FAILURE); 
		} 
		else
			printf("server acccept the client...\n");
		
		while( (b = recv(connfd, buff, 1024, 0)) > 0){
			printf("buff: %s\n", buff);
			// if( buff[strlen(buff) - 1] != 'T' || buff[strlen(buff) - 1] != 0)
			// 	buff[strlen(buff) - 1] = 0;
			//printf("buff: %s,PUT %d\n", buff, strcmp(buff, "PUT"));
			
			if(!strcmp(buff, (char*)"PUT")){
				b = recv(connfd, buff, 1024, 0);
				printf("filename: %s\n", buff);
				//fflush(stdout);
				char serverDir[100] = "./Server_files/";
				strcat(serverDir, buff);
				printf("%s, %ld\n", serverDir, sizeof(serverDir));
				FILE *fp = fopen(serverDir, "w");
				printf("FileCopy Created\n");
				if(fp != NULL){
			
					while( (b = recv(connfd, buff, 1024, 0)) > 0){
						printf("Recieved File: %s\n", buff);
						//fwrite(buff, 1, b, fp);
						fwrite(buff, 1, sizeof(buff), fp);
						fflush(stdout);
						break;
					}
				//printf("Received byte: %d\n",tot);
            		if (b<0)
               			perror("Error in tServerTCP: recieving bits < 0 \n");

            	fclose(fp);
				break;
				}
				else{
					printf("Error in tServerTCP: fp = NULL\n");
				}	
			}
			else if(!strcmp(buff, (char*)"GET")){
				//b = recv(connfd, buff, MAX_TCP_PACKET_SIZE, 0);
				struct stat st;
				b = recv(connfd, buff, 1024, 0);
				printf("filename: %s\n", buff);
				//fflush(stdout);
				char serverDir[100] = "./Server_files/";
				strcat(serverDir, buff);
				printf("%s, %ld\n", serverDir, sizeof(serverDir));
				FILE *fp = fopen(serverDir, "r");
				while( ( b = fread(buff, 1, st.st_size, fp)) > 0){
					printf("%s", buff);
					send(sockfd, buff, st.st_size, 0);
				}
				printf("Sent File: %s", buff);
				fclose(fp);
			}
		
		/* Original File Reciever
		FILE *fp = fopen("fileCopy1.txt", "wb");
		printf("FileCopy Created\n");
		if(fp != NULL){
			//read(connfd, buff, sizeof(buff));
			//printf("%s\n", buff);
			//fprintf(fp,"%s",buff);

			while( (b = recv(connfd, buff, MAX_TCP_PACKET_SIZE, 0)) > 0){
				printf("Size of buff: %ld\n", sizeof(buff));
				printf("Recieved File:\n%s", buff);
				//fflush(stdout);
				//fprintf(fp, "%s", buff);
				fwrite(buff, 1, sizeof(buff), fp);
				fflush(stdout);
			}
			//printf("Received byte: %d\n",tot);
            if (b<0)
               perror("Error in tServerTCP: recieving bits < 0 \n");

            fclose(fp);
		}
		else{
			printf("Error in tServerTCP: fp = NULL\n");
		}*/
		
		}
		close(connfd);
	}
	 

	// Function for chatting between client and server 
	//func(connfd); 

	// After chatting close the socket 
	close(sockfd); 
} 

#ifndef __cplusplus
}
#endif

