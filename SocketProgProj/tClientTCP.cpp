#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <sys/stat.h>
#include <map>
#include <arpa/inet.h>

#define MAX 80 
#define MAX_TCP_PACKET_SIZE 65535
#define PORT 8080 
#define RECV_PORT 5050
#define SA struct sockaddr 
#define USER "user"
#define PASSWORD "pass"
#define EXIT "EXT"
#define PUT "PUT"
#define GET "GET"
#define CONNECT "CNT"

char* ip   = (char*) "";
char* user = (char*) "";
char* pass = (char*) "";

void setup_tConnect(char* token);
void tConnect(int sockfd, sockaddr_in servaddr);
void tPut(char* token, FILE *fp, char* sendbuffer, int b, int sockfd);
void tGet(char* token, FILE *fp, sockaddr_in servaddr,char* sendbuffer, int b, int sockfd);

void func(int sockfd) { 
	char buff[MAX]; 
	int n; 
	for (;;) { 
		bzero(buff, sizeof(buff)); 
		printf("Enter the string : "); 
		n = 0; 
		while ((buff[n++] = getchar()) != '\n') 
			; 
		write(sockfd, buff, sizeof(buff)); 
		bzero(buff, sizeof(buff)); 
		read(sockfd, buff, sizeof(buff)); 
		printf("From Server : %s", buff); 
		if ((strncmp(buff, "exit", 4)) == 0) { 
			printf("Client Exit...\n"); 
			break; 
		} 
	} 
} 

/*
setup_tConnect(char* token): 
Description: parses input token and assigns ip, username and password.
Input: char * -> token
Output: void
*/
void setup_tConnect(char* token){
	int count = 0;
	printf("In setup_tconnect");
	while (token != NULL){
		printf("token %d: %s\n", count, token);
		if (count == 1)
			ip = token;
		else if (count == 2)
			user = token;
		else if (count == 3)
			pass = token;
		count++;
		token = strtok(NULL, " ");
	}
}

/*
tConnect(int sockfd, sockaddr_in servaddr):
Description: Sets up connection with Tiger Server
Input:	int -> sockfd
	    sockaddr_in -> servaddr
Output: void
*/
void tConnect(int sockfd, sockaddr_in servaddr){
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	servaddr.sin_port = htons(PORT); 

	// connect the client socket to server socket 
	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
		printf("connection with the server failed...\n"); 
		exit(EXIT_FAILURE); 
	} 
	else
		printf("connected to the server..\n"); 

}

int tPut_old(FILE *fp, char* sendbuffer, int b, int sockfd) {
	while( ( b = fread(sendbuffer, 1, sizeof(sendbuffer), fp)) > 0){
		send(sockfd, sendbuffer, b, 0);
	}
	printf("Sent File...\n");
}

/*
void tPut(char* token, FILE *fp, char* sendbuffer, int b, int sockfd):
Description: Opens file and sends it over to the Server
Input: 	FILE* -> fp
		char* -> sendbuffer
		int	  -> b
		int   -> sockfd
Output: void
*/
void tPut(char* token, FILE *fp, char* sendbuffer, int b, int sockfd) {
	char* fileName = (char*) "";
	struct stat st;
	int count  = 0;
	while (token != NULL){
		if (count == 1){
			fileName = token;
			break;
		}
		count++;
		printf("tPut token: %s", token);
		token = strtok(NULL, " ");
	}
	send(sockfd, fileName, sizeof(fileName)+1, 0);
	sleep(3);
	stat(fileName, &st);

	fp = fopen(fileName, "rb");
	if(fp == NULL){
		printf("Error: Unable to open file...\n");
		return;
	}
	while( ( b = fread(sendbuffer, 1, st.st_size, fp)) > 0){
		printf("%s", sendbuffer);
		send(sockfd, sendbuffer, st.st_size, 0);
	}
	// fscanf(fp,"%s",sendbuffer);
	// write(sockfd,sendbuffer,sizeof(sendbuffer));
	printf("Sent File...\n");
}

void tGet(char* token, FILE *fp, sockaddr_in servaddr,char* sendbuffer, int b, int sockfd){

	char* fileName = (char*)"";
	char getCommand[5] = "GET ";
	char clientDir[100] = "./Client_files/";
	char buff[MAX_TCP_PACKET_SIZE];
	socklen_t len;
	int connfd;
	int count  = 0;
	//fileName = (char*)malloc(100);
	printf("in tGET\n");
	while (token != NULL){
		if (count == 1){
			fileName = token;
			break;
		}
		count++;
		printf("tGet token: %s", token);
		token = strtok(NULL, " ");
	}
	send(sockfd, getCommand, strlen(getCommand), 0);
	sleep(1);
	send(sockfd, fileName, sizeof(fileName), 0);
	// free(fileName);
	strcat(clientDir, fileName);


	
	// // Binding newly created socket to given IP and verification 
	// if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
	// 	printf("socket bind failed...\n"); 
	// 	exit(EXIT_FAILURE); 
	// } 
	// else
	// 	printf("Socket successfully binded..\n"); 

	// // Now server is ready to listen and verification 
	// if ((listen(sockfd, 5)) != 0) { 
	// 	printf("Listen failed...\n"); 
	// 	return;
	// } 
	// else
	// 	printf("Server listening..\n"); 
	// len = sizeof(servaddr); 

	// // Accept the data packet from client and verification 
	read(sockfd, buff, MAX_TCP_PACKET_SIZE);
	printf("%s\n", buff);
	// while(1){
	// 	connfd = accept(sockfd, (SA*)&servaddr, &len); 
	// 	if (connfd < 0) { 
	// 		printf("server acccept failed...\n"); 
	// 		exit(EXIT_FAILURE); 
	// 	} 
	// 	else
	// 		printf("server acccept the client...\n");
	// 	FILE *fp = fopen(clientDir, "w");
	// 	printf("FileCopy Created\n");
	// 	if(fp != NULL){
			
	// 		while( (b = recv(connfd, buff, MAX_TCP_PACKET_SIZE, 0)) > 0){
	// 			printf("Recieved File: %s\n", buff);
	// 			fwrite(buff, 1, b, fp);
	// 		}
	// 		//printf("Received byte: %d\n",tot);
    //         if (b<0)
    //            perror("Error in tServerTCP: recieving bits < 0 \n");

    //         fclose(fp);
	// 	}
	// 	else{
	// 		printf("Error in tClientTCP: fp = NULL\n");
	// 	}
	// 	close(connfd);
	// }
}

int main() { 
	int sockfd, connfd, b; 
	struct sockaddr_in servaddr, cli; 
	char sendbuffer[MAX_TCP_PACKET_SIZE];
	char line[MAX];
	char lineCpy[MAX];
	FILE *fp;

	// socket create and varification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("socket creation failed...\n"); 
		exit(EXIT_FAILURE); 
	} 
	else
		printf("Socket successfully created..\n"); 
	bzero(&servaddr, sizeof(servaddr)); 
	printf("Welcome to the Tiger Server.\nPlease connect to the Server:");
	while(1){
		printf("$ ");
		fgets(line, MAX, stdin);
		if( line[strlen(line) - 1] == '\n')
			line[strlen(line) - 1] = 0;
		memcpy(lineCpy, line, sizeof(line));
		printf("Input: %s\n", line);
		printf("Input Copy: %s\n", lineCpy);
		if(!strcmp(line, "exit")){
			printf("Exiting...\n");
			fclose(fp);
			exit(EXIT_SUCCESS);
		}
			
		char * token = strtok(line, " ");
		printf("Token: %s, %d\n", token, strcmp(token, "tconnect"));
		if (!strcmp(token,"tconnect")){
			printf("in if");
			setup_tConnect(token);
			//tConnect(sockfd, servaddr);
			servaddr.sin_family = AF_INET; 
			servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
			servaddr.sin_port = htons(PORT); 
			// connect the client socket to server socket 
			if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
				printf("connection with the server failed...\n"); 
				exit(EXIT_FAILURE); 
			} 
			else
				printf("connected to the server..\n"); 

		}
		else if (!strcmp(token, "tput")){
			printf("In tPut\n");
			send(sockfd, PUT, sizeof(PUT), 0);
			//sleep(1);
			tPut(token, fp, sendbuffer, b, sockfd);
		}
		else if(!strcmp(token, "tget")){
			printf("In tGet\n");
			tGet(token, fp, servaddr, sendbuffer, b, sockfd);
		}
		else if (!strcmp(token, "exit")){
			printf("Exiting Server...");
			send(sockfd, "exit", strlen("exit"), 0);
			fclose(fp);
			break;
		}
	}
	// printf("Input: %s\n", line);
	// printf("First: %s\n", token);

	// assign IP, PORT 
	// servaddr.sin_family = AF_INET; 
	// servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	// servaddr.sin_port = htons(PORT); 

	// // connect the client socket to server socket 
	// if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
	// 	printf("connection with the server failed...\n"); 
	// 	exit(EXIT_FAILURE); 
	// } 
	// else
	// 	printf("connected to the server..\n"); 

	// function for chat 
	// func(sockfd); 

	// FILE *fp = fopen("./file1.txt", "rb");
	// if(fp == NULL){
	// 	printf("Error: Unable to open file...");
	// 	exit(EXIT_FAILURE);
	// }
	//tPut_old(fp, sendbuffer, b, sockfd);
	// Send file...
	/*
	while( ( b = fread(sendbuffer, 1, sizeof(sendbuffer), fp)) > 0){
		send(sockfd, sendbuffer, b, 0);
	}
	printf("Sent File...\n");
	*/

	// close the socket 
	
	close(sockfd); 
} 
