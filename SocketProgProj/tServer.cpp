#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <errno.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <map>
#include <sys/stat.h>
#include <arpa/inet.h>
#define MAX 80 
#define PORT 8080 
#define SEND_PORT 5050
#define SA struct sockaddr 
#define MAX_TCP_PACKET_SIZE 65535
#define USER "user"
#define PASS "pass"

#define FILENAME 

#ifndef __cplusplus
extern "C"
{
#endif

// typedef struct send_struct{
// 	char[4] command;
// 	char[50] fileName;
// 	signed int fileSize;
// 	char* fileContents;
// };

// typedef struct recv_struct{
// 	char[3] command;
// 	char[50] fileName;
// };

// Function designed for chat between client and server. 
void func(int sockfd) 
{ 
	char buff[MAX_TCP_PACKET_SIZE]; 
    char line[MAX_TCP_PACKET_SIZE];
	char lineCpy[MAX_TCP_PACKET_SIZE];
    char serverDir[100] = "./Server_files/";
    char* fileName = (char*)"";
	char* fileSize = (char*)"";
	FILE *fp;
	int n; 
    int tokCnt;
    struct stat st;
    
	// infinite loop for chat 
	for (;;) { 
		bzero(buff, MAX_TCP_PACKET_SIZE); 

		// read the message from client and copy it in line
		read(sockfd, line, sizeof(line));
        if( line[strlen(line) - 1] == '\n')
			line[strlen(line) - 1] = 0;
        printf("Line: %s\n", line);
        memcpy(lineCpy, line, sizeof(line));
        // exit command
        if(!strcmp(line, "exit")){
			printf("Exiting...\n");
			//fclose(fp);
			exit(EXIT_SUCCESS);
		}
        // split line into tokens using " " delimiter
        char * token = strtok(line, " ");
        // if tput command is receieved. Will take buffer and put into file
        if (!strcmp(token, "tput")){
			printf("Recieved tput Command...\n");
            tokCnt = 0;
            while (token != NULL){
				//printf("%d, ", tokCnt);
                if (tokCnt == 1){
			        fileName = token;
			        //break;
		        }
				if (tokCnt == 2){
					fileSize = token;
					break;
				}
                tokCnt++;
		        //printf("tPut token: %s", token);
		        token = strtok(NULL, " ");
	        }
			read(sockfd, buff, sizeof(buff));
            strcat(serverDir, fileName);
            fp = fopen(serverDir, "w");
			//printf("line fter parsing token..\n: %s\n", buff);
            //memcpy(buff, line, sizeof(line));
			int leng;
			//sscanf(fileSize, "%d", &leng);
			//printf("size: %s, %d", fileSize, leng);
			
            fwrite(buff, 1, sizeof(leng), fp);
			printf("Created File: %s\n", fileName);
            fclose(fp);
        }
        // if tput command is recieved. Will send File data to client.
        else if (!strcmp(token, "tget")){
			printf("Recieved Tget Command...\n");
            tokCnt = 0;
            while (token != NULL){
                if (tokCnt == 1){
			        fileName = token;
			        break;
		        }
                tokCnt++;
		        //printf("tGet token: %s", token);
		        token = strtok(NULL, " ");
	        }
			printf("tGet Filename: %s\n", fileName);
            strcat(serverDir, fileName);
            fp = fopen(serverDir, "r");
            stat(serverDir, &st);
			/*
			strcat(ClientDir, fileName);
            printf("pwd: %s\n", ClientDir);
            fp = fopen(ClientDir, "r");
            printf("%d", fp);
            stat(ClientDir, &st);
            memcpy(buff, lineCpy, sizeof(lineCpy));
            printf("buffer: %s\n", buff);
            char fileBuffer[st.st_size];
			char sizeStr[12];
			sprintf(sizeStr, "%ld", st.st_size);
			strcat(buff, " ");
            strcat(buff, sizeStr);
			write(sockfd, buff, sizeof(buff));
            fread(fileBuffer, 1, st.st_size, fp);
			fflush(stdout);
            printf("FileBuffer: %s\n", fileBuffer);
			*/
			//memcpy(buff, fileName, sizeof(fileName));
			char sizeStr[12];
			sprintf(sizeStr, "%ld", st.st_size);
			//strcat(buff, " ");
            //strcat(buff, sizeStr);
			write(sockfd, sizeStr, sizeof(sizeStr));
			char fileBuffer[st.st_size];
            fread(fileBuffer, 1, st.st_size, fp);

            write(sockfd, fileBuffer, sizeof(fileBuffer));
			printf("Sent file: %s\n", fileName);  
            fclose(fp);
        }
        //Clear Data
        bzero(line, sizeof(line));
        bzero(lineCpy, sizeof(lineCpy));
        bzero(serverDir, sizeof(serverDir));
		bzero(fileName, sizeof(fileName));
        memcpy(serverDir,"./Server_files/", sizeof("./Server_files/"));
	} 
} 

// Driver function...Socket main function with help from Geeks for Geeks.com
int main() 
{ 
	int sockfd, connfd;
    socklen_t len;
    struct sockaddr_in servaddr, cli; 

	// socket create and verification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("socket creation failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully created..\n"); 
	bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(PORT); 

	// Binding newly created socket 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) { 
		printf("socket bind failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully binded..\n"); 

	// Now server is ready to listen and verification 
	if ((listen(sockfd, 5)) != 0) { 
		printf("Listen failed...\n"); 
		exit(0); 
	} 
	else
		printf("Server listening..\n"); 
	len = sizeof(cli); 

	// Accept the data packet from client and verification 
	connfd = accept(sockfd, (SA*)&cli, &len); 
	if (connfd < 0) { 
		printf("server accept failed...\n"); 
		exit(0); 
	} 
	else
		printf("server accept the client...\n"); 

	// Function for chatting between client and server 
	func(connfd); 

	// After chatting close the socket 
	close(sockfd); 
} 



#ifndef __cplusplus
}
#endif