#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <errno.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <map>
#include <sys/stat.h>
#include <arpa/inet.h>
#define MAX 80 
#define PORT 8080 
#define SEND_PORT 5050
#define SA struct sockaddr 
#define MAX_TCP_PACKET_SIZE 65535
#define USER "user"
#define PASS "pass"

#define FILENAME 

#ifndef __cplusplus
extern "C"
{
#endif

// typedef struct send_struct{
// 	char[3] command;
// 	char[50] fileName;
// 	signed int fileSize;
// 	char* fileContents;
// };

// typedef struct recv_struct{
// 	char[3] command;
// 	char[50] fileName;
// };

void func(int sockfd) 
{ 
    char buff[MAX_TCP_PACKET_SIZE]; 
    char line[MAX];
	char lineCpy[MAX];
    char ClientDir[100] = "./Client_files/";
    char* fileName = (char*)"";
	FILE *fp;
	int n; 
    int tokCnt;
    struct stat st;

	for (;;) { 
		bzero(buff, sizeof(buff)); 
		printf("$: "); 
		n = 0; 
		while ((line[n++] = getchar()) != '\n') 
			; 
        if( line[strlen(line) - 1] == '\n')
			line[strlen(line) - 1] = 0;
        memcpy(lineCpy, line, MAX);
        if(!strcmp(line, "exit")){
			printf("Exiting...\n");
			write(sockfd, line, sizeof(buff));
			exit(EXIT_SUCCESS);
		}

        char * token = strtok(line, " ");
        if (!strcmp(token, "tput")){
            tokCnt = 0;
            while (token != NULL){
                if (tokCnt == 1){
			        fileName = token;
			        break;
		        }
                tokCnt++;
		        token = strtok(NULL, " ");
	        }
            strcat(ClientDir, fileName);
            //printf("pwd: %s\n", ClientDir);
            fp = fopen(ClientDir, "r");
            //printf("%d", fp);
            stat(ClientDir, &st);
            memcpy(buff, lineCpy, sizeof(lineCpy));
            //printf("buffer: %s\n", buff);
            char fileBuffer[st.st_size];
			char sizeStr[12];
			sprintf(sizeStr, "%ld", st.st_size);
			strcat(buff, " ");
            strcat(buff, sizeStr);
			write(sockfd, buff, sizeof(buff));
            fread(fileBuffer, 1, st.st_size, fp);
			fflush(stdout);
            //printf("FileBuffer: %s\n", fileBuffer);

			//strcat(buff, " ");
            //strcat(buff, fileBuffer);
            write(sockfd, fileBuffer, sizeof(fileBuffer));
			printf("Sent file %s to TServer...\n", fileName);
            fclose(fp);
        }
        else if (!strcmp(token, "tget")){
            tokCnt = 0;
            while (token != NULL){
                if (tokCnt == 1){
			        fileName = token;
			        break;
		        }
                tokCnt++;
		        //printf("tGet token: %s", token);
		        token = strtok(NULL, " ");
	        }
            write(sockfd, lineCpy, sizeof(lineCpy)); 
            strcat(ClientDir, fileName);
			fp = fopen(ClientDir, "w");
            read(sockfd, buff, 12); 
			//token = strtok(buff, " ");
			char* fileSizestr = buff;
			int fileSizeGet;
			sscanf(fileSizestr, "%d", &fileSizeGet );
			read(sockfd, buff, fileSizeGet);
            fwrite(buff, 1, fileSizeGet, fp);
            printf("File Recieved. Location: %s\n", ClientDir);
            fclose(fp);
        }
        bzero(line, sizeof(line));
        bzero(lineCpy, sizeof(lineCpy));
        bzero(ClientDir, sizeof(ClientDir));
		memcpy(ClientDir,"./Client_files/", sizeof("./Client_files/"));
        //ClientDir = "./Client_files/";
	} 
} 
// Driver function...Socket main function with help from Geeks for Geeks.com
int main() 
{ 
	int sockfd, connfd, n=0; 
	struct sockaddr_in servaddr, cli; 
    char line[MAX];
    printf("WELCOME TO TIGER FTP SERVER!\nConnect to the Server..\n$: "); 
	while ((line[n++] = getchar()) != '\n') 
		; 
	// socket create and varification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) { 
		printf("socket creation failed...\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully created..\n"); 
	bzero(&servaddr, sizeof(servaddr)); 

	// assign Server Addr. 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	servaddr.sin_port = htons(PORT); 

	// connect the client socket to server socket 
	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) { 
		printf("connection with the server failed...\n"); 
		exit(0); 
	} 
	else
		printf("connected to the server..\n"); 

	// function for chat 
	func(sockfd); 

	// close the socket 
	close(sockfd); 
} 

#ifndef __cplusplus
}
#endif