To compile the Code run ./tcompile.sh
It will return a TigerS and TigerC binary.
To run the Code, using two terminals, run ./TigerS and ./TigerC.
When asked for input, enter tconnect, tput <file>, tget <file>, exit

There are actually two versions of the code. One code unfortnately segfaults
where as the other the client is able to send the Server but the Client is unable to recieve.

This is because, the sizes of the buffers were set to the MAx available size of a TCP buffer.
example run:

./tcompile.sh
Terminal 1: $./TigerC
Terminal 2: $./TigerS

--or--
./tcompile.sh
Terminal 1: $tClient
Terminal 2: $tServer


